/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Healther;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FuLian
 */
public class LoginViewController implements Initializable {

    @FXML
    private Button startButton;

    /**
     * Initializes the controller class.
     * @throws java.io.IOException
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    

    @FXML
    private void startButtonOnAction(ActionEvent event) throws IOException {
        Runtime.getRuntime().exec("cmd /c start " + "http://www.fitbit.com/oauth2/authorize?response_type=code^&client_id=229XXM^&redirect_uri=http://yzujava.noip.me/~big/healther_OAuth2.php^&scope=activity%20heartrate%20sleep");
        Stage nowStage = (Stage) startButton.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("LineChartView.fxml"));
        Scene scene = new Scene(root);
        nowStage.setScene(scene);
        nowStage.show();
    }
    
}
