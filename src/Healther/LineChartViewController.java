/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Healther;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author FuLian
 */
public class LineChartViewController implements Initializable {

    @FXML
    private ListView dataList;
    @FXML
    private ListView durationList;
    @FXML
    private NumberAxis numberAxis;
    @FXML
    private CategoryAxis categoryAxis;
    @FXML
    private SplitPane mainView;
    @FXML
    private LineChart lineChart;
    @FXML
    private Button backButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dataList.setItems(FXCollections.observableArrayList(
                "Heartrate", "Steps", "Sleep", "Calories")
        );

        durationList.setItems(FXCollections.observableArrayList(
                "Week", "Month", "Season")
        );
        durationList.setDisable(true);
        mainView.setResizableWithParent(dataList, Boolean.FALSE);
    }

    @FXML
    private void selectData(MouseEvent event) {
        durationList.setDisable(false);
    }

    @FXML
    private void selectDuration(MouseEvent event) throws IOException {
        String option = (String) dataList.getSelectionModel().getSelectedItem();
        if (option.equals("Heartrate")) {
            genheartrateChart();
        } else if (option.equals("Steps")) {
            genStepChart();
        }
    }

    private void genheartrateChart() throws IOException {
        String timeOption = (String) durationList.getSelectionModel().getSelectedItem();
        lineChart.getData().clear();
        lineChart.setTitle("Heart rate");
        categoryAxis.setLabel("Date");
        numberAxis.setLabel("Heart rate");
        JSONObject json = null;
        if(timeOption.equals("Week")) {
            json = new JSONObject(DataAccesser.getData("activities/heart/date/2016-01-09/7d.json"));
        }
        else if(timeOption.equals("Month")) {
            json = new JSONObject(DataAccesser.getData("activities/heart/date/2016-01-09/1m.json"));
            lineChart.setPrefWidth(800);
        }
        else if(timeOption.equals("Season")) {
            json = new JSONObject(DataAccesser.getData("activities/heart/date/2016-01-09/3m.json"));
            lineChart.setPrefWidth(5000);
        }
        
        
        if(json == null) {
            return;
        }
        
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Rest Heartrate");
        json.getJSONArray("activities-heart").forEach((obj) -> {
            JSONObject tmp = new JSONObject(obj.toString());
            series1.getData().add(new XYChart.Data(tmp.getString("dateTime"), tmp.getJSONObject("value").getInt("restingHeartRate")));
        });
        lineChart.getData().add(series1);
        
        XYChart.Series series2 = new XYChart.Series();
        series1.setName("Rest Heartrate");
        json.getJSONArray("activities-heart").forEach((obj) -> {
            JSONObject tmp = new JSONObject(obj.toString());
            series1.getData().add(new XYChart.Data(tmp.getString("dateTime"), tmp.getJSONObject("value").getInt("restingHeartRate")));
        });
        lineChart.getData().add(series1);
        
    }
    
    private void genStepChart() throws IOException {
        String timeOption = (String) durationList.getSelectionModel().getSelectedItem();
        lineChart.getData().clear();
        lineChart.setTitle("Steps");
        categoryAxis.setLabel("Date");
        numberAxis.setLabel("Steps");
        JSONObject json = null;
        if(timeOption.equals("Week")) {
            json = new JSONObject(DataAccesser.getData("activities/steps/date/today/7d.json"));
        }
        else if(timeOption.equals("Month")) {
            json = new JSONObject(DataAccesser.getData("activities/steps/date/today/1m.json"));
        }
        else if(timeOption.equals("Season")) {
            json = new JSONObject(DataAccesser.getData("activities/steps/date/today/3m.json"));
        }
        
        
        if(json == null) {
            return;
        }
        
        XYChart.Series series = new XYChart.Series();
        series.setName("My Steps");
        json.getJSONArray("activities-steps").forEach((obj) -> {
            JSONObject tmp = new JSONObject(obj.toString());
            series.getData().add(new XYChart.Data(tmp.getString("dateTime"), tmp.getInt("value")));
        });
        lineChart.getData().add(series);
    }

    @FXML
    private void backButtonOnAction(ActionEvent event) throws IOException {
        Stage nowStage = (Stage) backButton.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("LoginView.fxml"));
        Scene scene = new Scene(root);
        nowStage.setScene(scene);
        nowStage.show();
    }

    @FXML
    private void lineChartOnMouseDragOver(MouseDragEvent event) {
        System.out.println("Drag");
    }

}
